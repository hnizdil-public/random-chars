# Random Chars

All of the following instructions are macOS specific.

Copy random characters matrix into clipboard:
```
python3 main.py | pbcopy
```

Insert copied matrix into Numbers sheet:
- Open `empty.numbers` file
- Right-click in `A1` cell and choose `Paste and Match Style`
- Choose `Duplicate` in the dialog that has just popped up (the file is read-only)
- When printing, set page margins to `36 pt` and Content Scale to `Fit`

## Preparing Empty Sheet

File `empty.numbers` was already properly formatted by taking following steps:

- Then Cmd-A to select whole table
- Remove title, header row and header column
- Set text alignment to center
- Set font to Fira Code Medium or other that differentiates between O, 0, 1, I, l and |
- Make column size equal to row size
