"""

Prints CSV matrix of random generated characters to stdout.

"""

import csv
import random
import string
import sys

size = 24
chars = string.ascii_letters + string.digits + string.punctuation
csv_writer = csv.writer(sys.stdout)

for i in range(size):
    row: list[string] = []
    for j in range(size):
        row.append(random.choice(chars))
    csv_writer.writerow(row)
